# Dev Days 2022

# Make Your Data Usable with Matillion ETL for Amazon Redshift

## **Overview**
Welcome to Matillion ETL! In this hands-on instruction, we will show the Matillion graphical UI in action. Users will be properly introduced to the Matillion-AWS-Redshift relationship as they drag and drop components, constructing an Extract-Load-Transform pipeline involving a domestic US flights dataset. Users will learn how to:
    - Extract data from an AWS S3 account
    - Leverage AWS Redshift best practices from within the Matillion UI
    - Utilize Matillion Transformation Jobs to make data useful

The session will mainly be comprised of the following two steps:
1. Creating a Matillion Instance via the Matillion Hub
2. Matillion Exercises

## **Creating a Matillion Instance via the Matillion Hub**
**Note:** If your organization already has a Matillion ETL for Redshift instance, please use that instance and desregard the **Creating a Matillion Instance via the Matillion Hub** instructions. 

**Instructions:**
1. [Register](https://documentation.matillion.com/docs/2568303) for a Hub account at [hub.matillion.com](http://hub.matillion.com/)
2. Follow [this video](https://drive.google.com/file/d/1qZM4cJxFvEdw7vLBxq2Krhrew6J0TTmQ/view?usp=sharing) or [this documentation](https://documentation.matillion.com/docs/2568302) for a walkthrough on creating a Matillion ETL for Redshift instance through the Hub.
3. The Hub will link to the AWS account signed in within the browser, and you can follow [this documentation](https://documentation.matillion.com/docs/2568307) for a full listing of steps on how to deploy within AWS. Note that you will need:
    - An IAM role associated with the instance with the roles listed [here](https://documentation.matillion.com/docs/2954248).
    - An AWS Security Group which allows incoming traffic to the instance on SSH, HTTP, and HTTPS ports.
4. Once the instance is launched it will then need to be [associated with the Hub](https://documentation.matillion.com/docs/2568308). There will be a prompt within the instance that you can simply follow to link back to the Hub.

## **Matillion Exercises**
**Orchestration**
1. [Create a project](https://documentation.matillion.com/docs/1991963)
    - Creates a space to create your jobs
    - Stores AWS and Redshift credentials for use by Matillion
2. [Alter Session WLM Slots](https://documentation.matillion.com/docs/2930333)
    - Set value of Slot Count to `4`
3. [Create Table Component](https://documentation.matillion.com/docs/2043153)
    - New Table Name: `flight_sample`
    - Create / Replace: `Create if not exists`
    - Distribution Style: `Auto`
    - Backup Table: `Yes`
    - Table Metadata: Upload Contents of [this sheet](https://docs.google.com/spreadsheets/d/1KWerej1l-OWwpCCiP8YR6sd2Pe9BjMb-A0e5D18TFr0/edit#gid=0) into Text Mode

3. [S3 Load](https://documentation.matillion.com/docs/2042349)
    - Target Table Name: `flight_sample`
    - S3 URL Location: `s3://mtln-public-data/`
    - S3 Object Prefix: `flights/flight_sample.csv.gz`
    - IAM Role ARN: `arn:aws:iam::<aws-account-id>:role/<role-name>`
    - Data File Type: `Delimited`
    - Delimiter `,`
    - Explicit IDs: `No`
    - S3 Bucket Region: `None`
    - Compression Method: `GZip`
    - Maximum Errors: `100000`
    - Date Format: `auto`
    - Time Format: `auto`
    - Ignore Header Rows: `1`
    - Accept Any Date: `Yes`
    - Ignore Blank Lines: `Yes`
    - Truncate Columns: `No`
    - Fill Record: `Yes`
    - Trim Blanks: `Yes`
    - Null As: `\\N`
    - Empty As Null: `Yes`
    - Blank As Null: `Yes`
    - Comp Update: `No`
    - Stat Update: `No`
    - Escape: `Yes`
    - Round Decimals: `Yes`
    - Manifest: `No`

4. [Database Query](https://documentation.matillion.com/docs/2147237)
    - Basic/Advanced Mode: `Advanced`
    - Database Type: `PostgreSQL`
    - Connection URL: `jdbc:postgresql://test-flight-library-psql.c4ttnsr9vh61.eu-west-1.rds.amazonaws.com/testflight`
    - Username: `readonly`
    - Password: `metl1234`
    - SQL Query: `select * from public.airports`
    - Schema: `[Environment Default]`
    - Target Table: `airports_postgres`
    - S3 Staging Area: `[Environment Default]`
    - Distribution Style: `Auto`
    - Concurrency Value: `2`
    - Concurrency Method: `Absolute`
    - Encryption Method: `None`

5. [And](https://documentation.matillion.com/docs/2043148)

6. [Create Transformation Job](https://documentation.matillion.com/docs/2037606)
    - Right-click the project in the Project Explorer and select `Create Transformation Job`
    - Drag the transformation job onto the workspace as the next step after the And Component
    - Double-click the transformation job to begin transforming the data which was just loaded

 

**Transformation**
1. [Table Input Component](https://documentation.matillion.com/docs/1991918)
    - Name: `Flights`
    - Table Name: `flight_sample`
    - Column Names: Select all Columns

2. [Table Input Component](https://documentation.matillion.com/docs/1991918)
    - Name: `Origin Airport`
    - Table Name: `airports_postgres`
    - Column Names: Select all Columns

3. [Table Input Component](https://documentation.matillion.com/docs/1991918)
    - Name: `Destination Airport`
    - Table Name: `airports_postgres`
    - Column Names: Select all Columns

4. [Join Component](https://documentation.matillion.com/docs/1991923)
    - Name: `Join Flights & Airports`
    - Main Table: `Flights`
    - Main Table Alias: `flights`
    - Joins
|

| Join Table | Join Alias | Join Type |
| ------ | ------ | ------ |
| Origin Airport | origin | Left |
| Origin Airport | destination | Left |
| flights.flightnum | FlightNum |

    - Join Expressions 
        - flights_Left_origin: `"flights"."origin" = "origin"."iata"`
        - flights_Left_destination: `"flights"."dest" = "destination"."iata"`
    - Output Columns
|

| Input Column | Output Column |
| ------ | ------ |
| origin.city | OriginCity |
| destination.city | DestCity |
| flights.flightnum | FlightNum |

5. [Aggregate Component](https://documentation.matillion.com/docs/1991880)
    - Groupings: `origincity`,`destcity`
    - Aggregations: `flightnum`,`Count`

6. [Rewrite Table Component](https://documentation.matillion.com/docs/1991941)
    - Target Table Name: `route_count`
